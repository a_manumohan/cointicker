package com.mn.cointicker.util

import io.reactivex.Scheduler

data class SchedulerProvider(val io: Scheduler, val mainThread: Scheduler)