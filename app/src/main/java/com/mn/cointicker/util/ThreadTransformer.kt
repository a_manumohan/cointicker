package com.mn.cointicker.util

import io.reactivex.FlowableTransformer

class ThreadTransformer(private val schedulerProvider: SchedulerProvider) {
    fun <T> applyFlowableTransformer(): FlowableTransformer<T, T> {
        return FlowableTransformer { o ->
            o.subscribeOn(schedulerProvider.io)
                .observeOn(schedulerProvider.mainThread)
        }
    }
}