package com.mn.cointicker.ui.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mn.cointicker.R
import com.mn.cointicker.api.model.TickerMessage
import com.mn.cointicker.base.BaseActivity
import com.mn.cointicker.base.BaseFragment
import com.mn.cointicker.ui.details.model.UiCurrencyPair
import com.mn.cointicker.ui.details.model.UiInput
import kotlinx.android.synthetic.main.currency_pair_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import java.lang.IllegalArgumentException

class CurrencyPairFragment : BaseFragment() {
    private val currencyPairViewModel: CurrencyPairViewModel by viewModel { parametersOf(getTickerMessage()) }

    private fun getTickerMessage(): TickerMessage {
        return arguments?.getParcelable(CURRENCY_PAIR_ID)
            ?: throw IllegalArgumentException("A tickerMessage instance should be passed to this fragment")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.currency_pair_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        subscribeToTickerUpdates()
        subscribeToInputUpdates()
    }

    private fun subscribeToTickerUpdates() {
        currencyPairViewModel.currencyPairUpdates()
            .observeForever {
                bindCurrencyPair(it)
            }
        currencyPairViewModel.start()
    }

    private fun subscribeToInputUpdates() {
        currencyPairViewModel.numberInput()
            .observeForever { bindNumberInput(it) }
    }

    private fun bindNumberInput(uiInput: UiInput) {
        currencyPairInputView?.bind(uiInput)
    }

    private fun bindCurrencyPair(currencyPair: UiCurrencyPair) {
        (activity as BaseActivity?).let {
            it?.supportActionBar?.title = currencyPair.currencyPair
        }
        currencyPairView?.bind(currencyPair)
    }

    companion object {
        private const val CURRENCY_PAIR_ID = "currency_pair_id"

        fun newInstance(tickerMessage: TickerMessage): CurrencyPairFragment {
            val args = Bundle()
            args.putParcelable(CURRENCY_PAIR_ID, tickerMessage)
            return CurrencyPairFragment().apply { arguments = args }
        }
    }
}