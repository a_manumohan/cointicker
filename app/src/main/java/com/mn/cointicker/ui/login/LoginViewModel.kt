package com.mn.cointicker.ui.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.mn.cointicker.base.BaseViewModel
import com.mn.cointicker.ui.login.model.InvalidData
import com.mn.cointicker.ui.login.model.LoginNavigation
import com.mn.cointicker.ui.login.model.ToTicker
import com.mn.cointicker.ui.login.model.UiLogin

class LoginViewModel : BaseViewModel() {
    private val loginLiveData = MutableLiveData<UiLogin>()
    private val navigationLiveData = MutableLiveData<LoginNavigation>()

    fun loginUpdates(): LiveData<UiLogin> = loginLiveData

    fun navigationUpdates(): LiveData<LoginNavigation> = navigationLiveData

    fun ready() {
        loginLiveData.value = getUiLogin()
    }

    private fun getUiLogin(): UiLogin {
        return UiLogin { username, password ->
            val verified = verifyCredentials(username, password)
            navigateOrError(verified)
        }
    }

    private fun navigateOrError(verified: Boolean) {
        navigationLiveData.value = if (verified) {
            ToTicker
        } else {
            InvalidData
        }
    }

    private fun verifyCredentials(username: String, password: String): Boolean {
        return username == USERNAME && password == PASSWORD
    }

    companion object {
        private const val USERNAME = "bitcoin"
        private const val PASSWORD = "coinworld"
    }
}
