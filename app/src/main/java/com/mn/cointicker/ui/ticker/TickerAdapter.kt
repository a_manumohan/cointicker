package com.mn.cointicker.ui.ticker

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mn.cointicker.ui.ticker.model.UiTicker

class TickerAdapter : RecyclerView.Adapter<TickerViewHolder>() {
    private val tickerItems = mutableListOf<UiTicker>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TickerViewHolder {
        val tickerView = TickerView(parent.context)
        tickerView.layoutParams = ViewGroup.LayoutParams(
            RecyclerView.LayoutParams.MATCH_PARENT,
            RecyclerView.LayoutParams.WRAP_CONTENT
        )
        return TickerViewHolder(tickerView)
    }

    override fun getItemCount() = tickerItems.size

    override fun onBindViewHolder(holder: TickerViewHolder, position: Int) {
        val uiTicker = tickerItems[position]
        holder.bind(uiTicker)
    }

    fun addOrUpdate(uiTicker: UiTicker) {
        val index = tickerItems.indexOfFirst { it.id == uiTicker.id }
        when (index) {
            -1 -> {
                tickerItems.add(uiTicker)
                updateItemAt(tickerItems.size - 1)
            }
            else -> {
                tickerItems.removeAt(index)
                tickerItems.add(index, uiTicker)
                updateItemAt(index)
            }
        }
    }

    private fun updateItemAt(index: Int) {
        notifyItemChanged(index)
    }
}

class TickerViewHolder(private val view: TickerView) : RecyclerView.ViewHolder(view) {
    fun bind(uiTicker: UiTicker) {
        view.bind(uiTicker)
    }
}