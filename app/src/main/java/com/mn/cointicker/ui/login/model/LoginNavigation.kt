package com.mn.cointicker.ui.login.model

sealed class LoginNavigation

object ToTicker : LoginNavigation()

object InvalidData : LoginNavigation()