package com.mn.cointicker.ui.ticker.model

import com.mn.cointicker.api.model.TickerMessage

sealed class TickerNavigation

data class CurrencyPairDetails(val tickerMessage: TickerMessage) : TickerNavigation()
