package com.mn.cointicker.ui.ticker

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.mn.cointicker.R
import com.mn.cointicker.api.model.TickerMessage
import com.mn.cointicker.base.BaseFragment
import com.mn.cointicker.ui.details.CurrencyPairActivity
import com.mn.cointicker.ui.ticker.model.CurrencyPairDetails
import kotlinx.android.synthetic.main.ticker_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class TickerFragment : BaseFragment() {
    private val tickerAdapter = TickerAdapter()

    private val tickerViewModel: TickerViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.ticker_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
    }

    private fun setupViews() {
        tickerRecyclerView.adapter = tickerAdapter
        tickerRecyclerView.layoutManager = LinearLayoutManager(context)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        subscribeToNavigationUpdates()
        subscribeToTickerUpdates()
    }

    private fun subscribeToNavigationUpdates() {
        tickerViewModel.navigation()
            .observeForever {
                when (it) {
                    is CurrencyPairDetails ->
                        navigateToCurrencyPairDetails(it.tickerMessage)
                }
            }
    }

    private fun subscribeToTickerUpdates() {
        tickerViewModel.tickerUpdates()
            .observeForever {
                tickerAdapter.addOrUpdate(it)
            }
        tickerViewModel.start()
    }

    private fun navigateToCurrencyPairDetails(tickerMessage: TickerMessage) {
        context?.let {
            startActivity(CurrencyPairActivity.intent(it, tickerMessage))
        }
    }

    companion object {
        fun newInstance() = TickerFragment()
    }
}
