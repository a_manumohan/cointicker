package com.mn.cointicker.ui.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.mn.cointicker.R
import com.mn.cointicker.base.BaseFragment
import com.mn.cointicker.ui.login.model.InvalidData
import com.mn.cointicker.ui.login.model.ToTicker
import com.mn.cointicker.ui.ticker.TickerActivity
import kotlinx.android.synthetic.main.login_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginFragment : BaseFragment() {
    private val loginViewModel: LoginViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.login_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        subscribeToLoginUpdates()
        subscribeToNavigationUpdates()
        loginViewModel.ready()
    }

    private fun subscribeToNavigationUpdates() {
        loginViewModel.navigationUpdates()
            .observeForever {
                when (it) {
                    is ToTicker -> navigateToTicker()
                    is InvalidData -> showInvalidDataError()
                }
            }
    }

    private fun subscribeToLoginUpdates() {
        loginViewModel.loginUpdates()
            .observeForever {
                loginView.bind(it)
            }
    }

    private fun showInvalidDataError() {
        Toast.makeText(context, R.string.invalid_login_data, Toast.LENGTH_SHORT).show()
    }

    private fun navigateToTicker() {
        activity?.let {
            startActivity(TickerActivity.intent(it))
            it.finish()
        }
    }

    companion object {
        fun newInstance() = LoginFragment()
    }
}
