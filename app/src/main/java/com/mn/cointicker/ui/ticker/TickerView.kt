package com.mn.cointicker.ui.ticker

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import com.mn.cointicker.R
import com.mn.cointicker.ui.ticker.model.IconDirection
import com.mn.cointicker.ui.ticker.model.UiTicker
import kotlinx.android.synthetic.main.view_ticker.view.*

class TickerView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {
    init {
        inflate(context, R.layout.view_ticker, this)
        lastTradePrice.setInAnimation(context, android.R.anim.slide_in_left)
        lastTradePrice.setOutAnimation(context, android.R.anim.slide_out_right)
    }

    fun bind(uiTicker: UiTicker) {
        currency.text = uiTicker.currencyPair
        lastTradePrice.setText(uiTicker.lastPrice)
        valueAsk.text = uiTicker.ask
        valueBid.text = uiTicker.bid
        bindIcon(uiTicker)
        setOnClickListener { uiTicker.clickAction() }
    }

    private fun bindIcon(uiTicker: UiTicker) {
        when (uiTicker.iconDirection) {
            IconDirection.UP -> indicatorIcon.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.ic_arrow_upward_green_48dp
                )
            )
            IconDirection.DOWN -> indicatorIcon.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.ic_arrow_downward_red_48dp
                )
            )
        }
    }
}