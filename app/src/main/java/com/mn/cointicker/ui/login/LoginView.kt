package com.mn.cointicker.ui.login

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import com.mn.cointicker.R
import com.mn.cointicker.ui.login.model.UiLogin
import kotlinx.android.synthetic.main.view_login.view.*

class LoginView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {
    private var action: ((String, String) -> Unit)? = null

    init {
        inflate(context, R.layout.view_login, this)
        loginButton.setOnClickListener {
            val username = usernameView.text.toString().trim()
            val password = passwordView.text.toString().trim()
            action?.invoke(username, password)
        }
    }

    fun bind(uiLogin: UiLogin) {
        action = uiLogin.action
    }
}