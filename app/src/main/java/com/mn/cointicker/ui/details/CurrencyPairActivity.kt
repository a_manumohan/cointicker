package com.mn.cointicker.ui.details

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.mn.cointicker.R
import com.mn.cointicker.api.model.TickerMessage
import com.mn.cointicker.base.BaseActivity
import java.lang.IllegalArgumentException

class CurrencyPairActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.currency_pair_activity)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, CurrencyPairFragment.newInstance(getTickerMessage()))
                .commitNow()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun getTickerMessage(): TickerMessage {
        return intent.extras?.getParcelable(CURRENCY_PAIR_ID)
            ?: throw IllegalArgumentException("A tickerMessage instance should be passed to this activity")
    }

    companion object {
        private const val CURRENCY_PAIR_ID = "currency_pair_id"

        fun intent(context: Context, tickerMessage: TickerMessage): Intent {
            return Intent(context, CurrencyPairActivity::class.java)
                .apply { putExtra(CURRENCY_PAIR_ID, tickerMessage) }
        }
    }
}