package com.mn.cointicker.ui.details

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import com.mn.cointicker.R
import com.mn.cointicker.ui.details.model.UiCurrencyPair
import com.mn.cointicker.ui.ticker.model.IconDirection
import kotlinx.android.synthetic.main.view_currency_pair.view.*

class CurrencyPairView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {
    init {
        inflate(context, R.layout.view_currency_pair, this)
        currentPrice.setInAnimation(context, android.R.anim.slide_in_left)
        currentPrice.setOutAnimation(context, android.R.anim.slide_out_right)
    }

    fun bind(uiCurrencyPair: UiCurrencyPair) {
        currencyPair.text = uiCurrencyPair.currencyPair
        currentPrice.setText(uiCurrencyPair.currentPrice)
        valueAsk.text = uiCurrencyPair.ask
        valueBid.text = uiCurrencyPair.bid
        valueBaseVol.text = uiCurrencyPair.baseVol
        valueQuoteVol.text = uiCurrencyPair.quoteVol
        bindIcon(uiCurrencyPair.iconDirection)
    }

    private fun bindIcon(iconDirection: IconDirection) {
        when (iconDirection) {
            IconDirection.UP -> indicatorIcon.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.ic_arrow_upward_green_48dp
                )
            )
            IconDirection.DOWN -> indicatorIcon.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.ic_arrow_downward_red_48dp
                )
            )
        }
    }
}