package com.mn.cointicker.ui.ticker

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.mn.cointicker.api.model.Content
import com.mn.cointicker.api.model.TICKER_SUBSCRIBE
import com.mn.cointicker.api.model.TickerMessage
import com.mn.cointicker.base.BaseViewModel
import com.mn.cointicker.repository.TickerRepository
import com.mn.cointicker.ui.ticker.model.CurrencyPairDetails
import com.mn.cointicker.ui.ticker.model.IconDirection
import com.mn.cointicker.ui.ticker.model.TickerNavigation
import com.mn.cointicker.ui.ticker.model.UiTicker
import com.mn.cointicker.util.CurrencyMapper
import com.tinder.scarlet.WebSocket
import java.math.BigDecimal

class TickerViewModel(private val tickerRepository: TickerRepository) : BaseViewModel() {
    private val tickerLiveData = MutableLiveData<UiTicker>()
    private val navigationLiveData = MutableLiveData<TickerNavigation>()
    private val tickerMessages = mutableListOf<TickerMessage>()

    fun tickerUpdates(): LiveData<UiTicker> = tickerLiveData

    fun navigation(): LiveData<TickerNavigation> = navigationLiveData

    fun start() {
        disposable.add(tickerRepository.connectToServer()
            .subscribe {
                when (it) {
                    is WebSocket.Event.OnConnectionOpened<*> ->
                        tickerRepository.subscribeToTicker(TICKER_SUBSCRIBE)
                }
            })
        disposable.add(tickerRepository.getTickerUpdates()
            .filter { it is TickerMessage }
            .map { it as TickerMessage }
            .doOnNext {
                addOrUpdate(it)
            }
            .map { getUiTicker(it) }
            .subscribe {
                tickerLiveData.value = it
            })
    }

    private fun addOrUpdate(tickerMessage: TickerMessage) {
        val previousMessage = tickerMessages.find { it.content.currencyPairId == it.content.currencyPairId }
        previousMessage?.let {
            tickerMessages.remove(it)
        }
        tickerMessages.add(tickerMessage)
    }

    private fun getUiTicker(tickerMessage: TickerMessage): UiTicker {
        val content = tickerMessage.content
        val currencyPair = CurrencyMapper.getCurrencyPair(content.currencyPairId)
        return UiTicker(
            content.currencyPairId,
            currencyPair,
            content.lastTradePrice,
            content.lowestAsk,
            content.highestBid,
            getIconDirection(content)
        ) {
            messageSelected(tickerMessage)
        }
    }

    private fun messageSelected(tickerMessage: TickerMessage) {
        navigationLiveData.value = CurrencyPairDetails(tickerMessage)
    }

    private fun getIconDirection(currentContent: Content): IconDirection {
        val changePercent = BigDecimal(currentContent.variance)
        return when (changePercent.signum()) {
            -1 -> IconDirection.DOWN
            else -> IconDirection.UP
        }
    }
}
