package com.mn.cointicker.ui.ticker.model

data class UiTicker(
    val id: Int,
    val currencyPair: String,
    val lastPrice: String,
    val ask: String,
    val bid: String,
    val iconDirection: IconDirection,
    val clickAction: () -> Unit
)

enum class IconDirection {
    UP,
    DOWN
}