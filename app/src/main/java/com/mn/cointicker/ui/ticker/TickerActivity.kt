package com.mn.cointicker.ui.ticker

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.mn.cointicker.R
import com.mn.cointicker.base.BaseActivity

class TickerActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.ticker_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, TickerFragment.newInstance())
                .commitNow()
        }
    }

    companion object {
        fun intent(context: Context): Intent {
            return Intent(context, TickerActivity::class.java)
        }
    }
}
