package com.mn.cointicker.ui.details.model

data class UiInput(
    val number: String,
    val action: (number: String) -> Unit
)