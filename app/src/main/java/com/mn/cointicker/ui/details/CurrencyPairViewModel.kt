package com.mn.cointicker.ui.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.mn.cointicker.api.model.TICKER_SUBSCRIBE
import com.mn.cointicker.api.model.TickerMessage
import com.mn.cointicker.base.BaseViewModel
import com.mn.cointicker.repository.TickerRepository
import com.mn.cointicker.ui.details.model.UiCurrencyPair
import com.mn.cointicker.ui.details.model.UiInput
import com.mn.cointicker.ui.ticker.model.IconDirection
import com.mn.cointicker.util.CurrencyMapper
import com.tinder.scarlet.WebSocket
import java.math.BigDecimal

class CurrencyPairViewModel(private val tickerMessage: TickerMessage, private val tickerRepository: TickerRepository) :
    BaseViewModel() {
    private val currencyPairLiveData = MutableLiveData<UiCurrencyPair>()
    private val numberInputLiveData = MutableLiveData<UiInput>()
    private lateinit var pivotNumber: BigDecimal

    fun currencyPairUpdates(): LiveData<UiCurrencyPair> = currencyPairLiveData

    fun numberInput(): LiveData<UiInput> = numberInputLiveData

    init {
        updatePivotNumber(tickerMessage.content.lastTradePrice)
    }

    fun start() {
        initViews()
        disposable.add(tickerRepository.connectToServer()
            .subscribe {
                when (it) {
                    is WebSocket.Event.OnConnectionOpened<*> ->
                        tickerRepository.subscribeToTicker(TICKER_SUBSCRIBE)
                }
            })

        disposable.add(
            tickerRepository.getTickerUpdates()
                .filter { it is TickerMessage }
                .map { it as TickerMessage }
                .filter { it.content.currencyPairId == tickerMessage.content.currencyPairId }
                .map { getUiCurrencyPair(it, pivotNumber) }
                .subscribe {
                    currencyPairLiveData.value = it
                }
        )
    }

    private fun initViews() {
        currencyPairLiveData.value = getUiCurrencyPair(tickerMessage, pivotNumber)
        numberInputLiveData.value = UiInput(tickerMessage.content.lastTradePrice, updateNumberAction)
    }

    private fun updatePivotNumber(number: String) {
        val sanitizedNumber = if (number.isBlank()) {
            numberInputLiveData.value = UiInput("0", updateNumberAction)
            "0"
        } else {
            number
        }
        pivotNumber = BigDecimal(sanitizedNumber)
    }

    private fun getUiCurrencyPair(message: TickerMessage, pivot: BigDecimal): UiCurrencyPair {
        val content = message.content
        return UiCurrencyPair(
            CurrencyMapper.getCurrencyPair(content.currencyPairId),
            content.lastTradePrice,
            content.lowestAsk,
            content.highestBid,
            content.baseVol,
            content.quoteVol,
            getIconDirection(content.lastTradePrice, pivot)
        )
    }

    private fun getIconDirection(lastTradePrice: String, pivot: BigDecimal): IconDirection {
        val currentPrice = BigDecimal(lastTradePrice)
        return when (currentPrice.compareTo(pivot)) {
            -1 -> IconDirection.DOWN
            else -> IconDirection.UP
        }
    }

    private val updateNumberAction: (String) -> Unit = { number -> updatePivotNumber(number) }
}