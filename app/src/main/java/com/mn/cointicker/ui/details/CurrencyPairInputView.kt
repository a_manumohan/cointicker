package com.mn.cointicker.ui.details

import android.content.Context
import android.util.AttributeSet
import android.view.inputmethod.EditorInfo
import android.widget.FrameLayout
import com.mn.cointicker.R
import com.mn.cointicker.ui.details.model.UiInput
import kotlinx.android.synthetic.main.view_currency_pair_input.view.*

class CurrencyPairInputView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {
    private var doneAction: ((String) -> Unit)? = null

    init {
        inflate(context, R.layout.view_currency_pair_input, this)
        pivotNumber.setOnEditorActionListener { _, actionId, _ ->
            when (actionId) {
                EditorInfo.IME_ACTION_DONE -> {
                    val number = pivotNumber.text.toString().trim()
                    doneAction?.invoke(number)
                    true
                }
                else -> false
            }
        }
    }

    fun bind(uiInput: UiInput) {
        doneAction = uiInput.action
        pivotNumber.setText(uiInput.number)
        refreshButton.setOnClickListener {
            val number = pivotNumber.text.toString().trim()
            uiInput.action(number)
        }
    }
}