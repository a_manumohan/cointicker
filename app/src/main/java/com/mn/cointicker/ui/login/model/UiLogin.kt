package com.mn.cointicker.ui.login.model

data class UiLogin(val action: (String, String) -> Unit)