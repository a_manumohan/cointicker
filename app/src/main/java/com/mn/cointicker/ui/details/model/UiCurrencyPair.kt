package com.mn.cointicker.ui.details.model

import com.mn.cointicker.ui.ticker.model.IconDirection

data class UiCurrencyPair(
    val currencyPair: String,
    val currentPrice: String,
    val ask: String,
    val bid: String,
    val baseVol: String,
    val quoteVol: String,
    val iconDirection: IconDirection
)
