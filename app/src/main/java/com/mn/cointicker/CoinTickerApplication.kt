package com.mn.cointicker

import android.app.Application
import com.mn.cointicker.di.modules
import org.koin.android.ext.android.startKoin

class CoinTickerApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        setupKoin()
    }

    private fun setupKoin() {
        startKoin(this, modules)
    }
}