package com.mn.cointicker.base

import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity()