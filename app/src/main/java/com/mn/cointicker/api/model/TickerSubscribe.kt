package com.mn.cointicker.api.model

data class TickerSubscribe(
    val command: String,
    val channel: String
)

val TICKER_SUBSCRIBE = TickerSubscribe("subscribe", "1002")