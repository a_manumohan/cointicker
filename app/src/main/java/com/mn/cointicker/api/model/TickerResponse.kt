package com.mn.cointicker.api.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

sealed class TickerResponse

data class AckMessage(val id: String, val status: Int) : TickerResponse()

@Parcelize
data class TickerMessage(
    val id: String,
    val content: Content
) : TickerResponse(), Parcelable

@Parcelize
data class Content(
    val currencyPairId: Int,
    val lastTradePrice: String,
    val lowestAsk: String,
    val highestBid: String,
    val variance: String, // % change in the last 24 hours
    val baseVol: String, // base currency volume in last 24 hours
    val quoteVol: String, // quote currency volume in last 24 hours
    val isFrozen: Boolean,
    val highestTradePrice: String,
    val lowestTradePrice: String
) : Parcelable