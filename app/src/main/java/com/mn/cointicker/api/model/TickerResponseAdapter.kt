package com.mn.cointicker.api.model

import com.google.gson.JsonArray
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type

object TickerResponseAdapter : JsonDeserializer<TickerResponse> {
    override fun deserialize(element: JsonElement, p1: Type, p2: JsonDeserializationContext): TickerResponse {
        return when (element) {
            is JsonArray -> parseTickerResponse(element)
            else -> throw IllegalArgumentException("Expected array. Got " + element::javaClass)
        }
    }

    private fun parseTickerResponse(array: JsonArray): TickerResponse {
        return when {
            array.size() == 2 -> getAckMessage(array)
            array.size() > 2 -> getTickerMessage(array)
            else -> throw IllegalArgumentException("Not enough items in array ")
        }
    }

    private fun getTickerMessage(array: JsonArray): TickerMessage {
        return TickerMessage(
            array[0].asString,
            getContent(array[2])
        )
    }

    private fun getContent(array: JsonElement): Content {
        return when (array) {
            is JsonArray -> Content(
                array[0].asInt,
                array[1].asString,
                array[2].asString,
                array[3].asString,
                array[4].asString,
                array[5].asString,
                array[6].asString,
                array[7].asInt != 0,
                array[8].asString,
                array[9].asString
            )
            else -> throw IllegalArgumentException("Content should be an array ")
        }
    }

    private fun getAckMessage(array: JsonArray): AckMessage {
        return AckMessage(array[0].asString, array[1].asInt)
    }
}