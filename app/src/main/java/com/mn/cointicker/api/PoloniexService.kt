package com.mn.cointicker.api

import com.mn.cointicker.api.model.TickerResponse
import com.mn.cointicker.api.model.TickerSubscribe
import com.tinder.scarlet.WebSocket
import com.tinder.scarlet.ws.Receive
import com.tinder.scarlet.ws.Send
import io.reactivex.Flowable

interface PoloniexService {
    @Receive
    fun observeConnection(): Flowable<WebSocket.Event>

    @Send
    fun sendSubscribe(tickerSubscribe: TickerSubscribe)

    @Receive
    fun observeTicker(): Flowable<TickerResponse>
}