package com.mn.cointicker.repository

import com.mn.cointicker.api.PoloniexService
import com.mn.cointicker.api.model.TickerResponse
import com.mn.cointicker.api.model.TickerSubscribe
import com.mn.cointicker.util.ThreadTransformer
import com.tinder.scarlet.WebSocket
import io.reactivex.Flowable

class TickerRepository(
    private val poloniexService: PoloniexService,
    private val threadTransformer: ThreadTransformer
) {
    fun connectToServer(): Flowable<WebSocket.Event> {
        return poloniexService.observeConnection()
            .compose(threadTransformer.applyFlowableTransformer())
    }

    fun subscribeToTicker(tickerSubscribe: TickerSubscribe) {
        poloniexService.sendSubscribe(tickerSubscribe)
    }

    fun getTickerUpdates(): Flowable<TickerResponse> {
        return poloniexService.observeTicker()
            .compose(threadTransformer.applyFlowableTransformer())
    }
}