package com.mn.cointicker.di

import com.google.gson.GsonBuilder
import com.mn.cointicker.CoinTickerApplication
import com.mn.cointicker.R
import com.mn.cointicker.api.PoloniexService
import com.mn.cointicker.api.model.TickerResponse
import com.mn.cointicker.api.model.TickerResponseAdapter
import com.tinder.scarlet.MessageAdapter
import com.tinder.scarlet.Scarlet
import com.tinder.scarlet.StreamAdapter
import com.tinder.scarlet.lifecycle.android.AndroidLifecycle
import com.tinder.scarlet.messageadapter.gson.GsonMessageAdapter
import com.tinder.scarlet.retry.BackoffStrategy
import com.tinder.scarlet.retry.ExponentialWithJitterBackoffStrategy
import com.tinder.scarlet.streamadapter.rxjava2.RxJava2StreamAdapterFactory
import com.tinder.scarlet.websocket.okhttp.newWebSocketFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module.module

val appModule = module {

    single { HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY) as Interceptor }

    single {
        GsonBuilder()
            .registerTypeAdapter(TickerResponse::class.java, TickerResponseAdapter)
            .create()
    }
    single {
        OkHttpClient.Builder()
            .addInterceptor(get())
            .build()
    }

    single {
        get<OkHttpClient>().newWebSocketFactory(androidContext().resources.getString(R.string.base_url))
    }
    single {
        GsonMessageAdapter.Factory(get()) as MessageAdapter.Factory
    }
    single {
        RxJava2StreamAdapterFactory() as StreamAdapter.Factory
    }
    single {
        ExponentialWithJitterBackoffStrategy(500, 500) as BackoffStrategy
    }
    single {
        val application = androidContext().applicationContext as CoinTickerApplication
        AndroidLifecycle.ofApplicationForeground(application)
    }

    single {
        Scarlet.Builder()
            .webSocketFactory(get())
            .addMessageAdapterFactory(get())
            .addStreamAdapterFactory(get())
            .backoffStrategy(get())
            .lifecycle(get())
            .build()
    }
    single {
        get<Scarlet>().create() as PoloniexService
    }
}

val modules = listOf(
    appModule,
    repositoryModule,
    viewModelModule
)