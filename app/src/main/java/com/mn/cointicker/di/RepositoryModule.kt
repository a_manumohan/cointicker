package com.mn.cointicker.di

import com.mn.cointicker.repository.TickerRepository
import com.mn.cointicker.util.SchedulerProvider
import com.mn.cointicker.util.ThreadTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.koin.dsl.module.module

val repositoryModule = module {
    single { SchedulerProvider(Schedulers.io(), AndroidSchedulers.mainThread()) }

    single { ThreadTransformer(get()) }

    factory {
        TickerRepository(get(), get())
    }
}