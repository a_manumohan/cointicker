package com.mn.cointicker.di

import com.mn.cointicker.api.model.TickerMessage
import com.mn.cointicker.ui.details.CurrencyPairViewModel
import com.mn.cointicker.ui.login.LoginViewModel
import com.mn.cointicker.ui.ticker.TickerViewModel
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val viewModelModule = module {
    viewModel { LoginViewModel() }
    viewModel { TickerViewModel(get()) }
    viewModel { (tickerMessage: TickerMessage) ->
        CurrencyPairViewModel(tickerMessage, get())
    }
}